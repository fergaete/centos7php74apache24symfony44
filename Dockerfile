FROM centos:7

RUN mkdir -p /app

# Install dependencies
RUN yum -y install curl wget vim nfs-utils git zip
RUN yum -y install epel-release

# Install Apache
RUN yum -y install httpd
RUN yum -y install mod_ssl

# Install Supervidord
RUN yum -y install supervisor

# Configure Apache
COPY apache/00-base.conf /etc/httpd/conf.modules.d/00-base.conf

ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/httpd
ENV APACHE_LOCK_DIR /var/lock/httpd
ENV APACHE_PID_FILE /var/run/httpd.pid
ENV SYMFONY_ENV dev

# Update the default apache site with the config we created.
COPY apache/apache-config.conf /etc/httpd/sites-enabled/000-default.conf
COPY apache/apache-config-secure.conf /etc/httpd/sites-enabled/001-default-ssl.conf
RUN sed -i -e 's/\/var\/www\/html/\/app\/public/g' /etc/httpd/conf/httpd.conf

# Install PHP
RUN rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-7.rpm
RUN yum -y --enablerepo=remi --enablerepo=remi-php74 install php php-pgsql php-mbstring php-posix php-xml php-intl
RUN sed -i -e 's/;date.timezone = /date.timezone = "UTC"/g' /etc/php.ini
RUN sed -i -e 's/short_open_tag = Off/short_open_tag = On/g' /etc/php.ini
RUN sed -i -e 's/expose_php = On/expose_php = Off/g' /etc/php.ini
RUN yum-config-manager --enable remi-php74
RUN yum -y install php-opcache php-pecl-apcu php-xdebug
RUN yum -y install zip unzip php-zip
RUN yum -y install php-amqp

# Configure PHP
RUN echo "xdebug.idekey = PHPSTORM" >> /etc/php.d/xdebug.ini \
&& echo "xdebug.default_enable = 0" >> /etc/php.d/xdebug.ini \
&& echo "xdebug.remote_enable = 1" >> /etc/php.d/xdebug.ini \
&& echo "xdebug.remote_autostart = 0" >> /etc/php.d/xdebug.ini \
&& echo "xdebug.remote_connect_back = 0" >> /etc/php.d/xdebug.ini \
&& echo "xdebug.remote_port = 9000" >> /etc/php.d/xdebug.ini \
&& echo "xdebug.profiler_enable = 0" >> /etc/php.d/xdebug.ini

RUN echo "opcache.enable_cli = 1" >> /etc/php.d/opcache.ini \
&& echo "opcache.memory_consumption = 128" >> /etc/php.d/opcache.ini \
&& echo "opcache.interned_strings_buffer = 8" >> /etc/php.d/opcache.ini \
&& echo "opcache.max_accelerated_files = 4000" >> /etc/php.d/opcache.ini \
&& echo "opcache.revalidate_freq = 60" >> /etc/php.d/opcache.ini \
&& echo "opcache.fast_shutdown = 1" >> /etc/php.d/opcache.ini

RUN echo "apc.enabled = 1" >> /etc/php.d/apcu.ini \
&& echo  "apc.shm_size = 128M" >> /etc/php.d/apcu.ini \
&& echo  "apc.ttl = 3600" >> /etc/php.d/apcu.ini \
&& echo  "apc.user_ttl = 7200" >> /etc/php.d/apcu.ini \
&& echo  "apc.gc_ttl = 3600" >> /etc/php.d/apcu.ini

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer --version

WORKDIR /app

EXPOSE 80
EXPOSE 443
ENTRYPOINT ["/usr/sbin/httpd", "-D", "FOREGROUND"]
